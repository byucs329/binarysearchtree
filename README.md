This repository contains a simple binary search tree implementation and some accompanying JUnit Jupiter tests. The no-tests branch has the implementation and an empty class suitable for JUnit 5 tests; as such, it is suitable for in-class demos.

Because this repository's history contains test cases, it is not suitable as the basis for a homework assignment.
