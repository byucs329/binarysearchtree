package edu.byu.yc.test_examples;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import edu.byu.yc.test_examples.BSTSet.Parent;

@DisplayName("Basic BST tests")
public class BSTSetTests {
    BSTSet tree;

    @BeforeEach
    public void setup() throws Exception {
        tree = new BSTSet();
    }

    @AfterEach
    public void teardown() throws Exception {
        tree = null;
    }

    @Test
    @DisplayName("Add tests")
    public void addTests() {
        assertFalse(tree.contains(3));
        assertTrue(tree.add(3));
        assertTrue(tree.contains(3));
        assertFalse(tree.add(3));
        assertFalse(tree.contains(2));
        assertTrue(tree.add(2));
        assertTrue(tree.contains(2));
        assertFalse(tree.add(2));
        assertFalse(tree.add(3));
        assertFalse(tree.add(2));
        assertTrue(tree.contains(3));
        assertTrue(tree.contains(2));
    }

    @Test
    @DisplayName("Deletion test - child first")
    public void deleteR() {
        assertFalse(tree.contains(3));
        assertTrue(tree.add(3));
        assertTrue(tree.contains(3));
        assertFalse(tree.add(3));

        assertFalse(tree.contains(2));
        assertTrue(tree.add(2));
        assertTrue(tree.contains(2));
        assertFalse(tree.add(2));

        assertFalse(tree.contains(6));
        assertTrue(tree.add(6));
        assertTrue(tree.contains(6));
        assertFalse(tree.add(6));

        assertFalse(tree.contains(4));
        assertTrue(tree.add(4));
        assertTrue(tree.contains(4));
        assertFalse(tree.add(4));

        assertFalse(tree.contains(5));
        assertTrue(tree.add(5));
        assertTrue(tree.contains(5));
        assertFalse(tree.add(5));

        assertFalse(tree.add(3));
        assertFalse(tree.add(2));
        assertFalse(tree.add(4));
        assertFalse(tree.add(5));

        assertTrue(tree.contains(3));
        assertTrue(tree.contains(2));
        assertTrue(tree.contains(4));
        assertTrue(tree.contains(5));

        assertTrue(tree.delete(3));
        assertFalse(tree.delete(3));
        assertFalse(tree.contains(3));

        assertTrue(tree.delete(4));
        assertFalse(tree.delete(4));
        assertFalse(tree.contains(4));

        assertTrue(tree.delete(5));
        assertFalse(tree.delete(5));
        assertFalse(tree.contains(5));

        assertTrue(tree.delete(2));
        assertFalse(tree.delete(2));
        assertFalse(tree.contains(2));
    }

    @Test
    @DisplayName("Deletion test - child first")
    public void deleteRLL() {
        assertFalse(tree.contains(3));
        assertTrue(tree.add(3));
        assertTrue(tree.contains(3));
        assertFalse(tree.add(3));

        assertFalse(tree.contains(2));
        assertTrue(tree.add(2));
        assertTrue(tree.contains(2));
        assertFalse(tree.add(2));

        assertFalse(tree.contains(1));
        assertTrue(tree.add(1));
        assertTrue(tree.contains(1));
        assertFalse(tree.add(1));

        assertFalse(tree.contains(6));
        assertTrue(tree.add(6));
        assertTrue(tree.contains(6));
        assertFalse(tree.add(6));

        assertFalse(tree.contains(5));
        assertTrue(tree.add(5));
        assertTrue(tree.contains(5));
        assertFalse(tree.add(5));

        assertFalse(tree.contains(4));
        assertTrue(tree.add(4));
        assertTrue(tree.contains(4));
        assertFalse(tree.add(4));

        assertFalse(tree.add(3));
        assertFalse(tree.add(2));
        assertFalse(tree.add(4));
        assertFalse(tree.add(5));

        assertTrue(tree.contains(3));
        assertTrue(tree.contains(2));
        assertTrue(tree.contains(4));
        assertTrue(tree.contains(5));

        assertTrue(tree.delete(3));
        assertFalse(tree.delete(3));
        assertFalse(tree.contains(3));

        assertTrue(tree.delete(6));
        assertFalse(tree.delete(6));
        assertFalse(tree.contains(6));

        assertTrue(tree.delete(5));
        assertFalse(tree.delete(5));
        assertFalse(tree.contains(5));

        assertTrue(tree.delete(4));
        assertFalse(tree.delete(4));
        assertFalse(tree.contains(4));

        assertTrue(tree.delete(1));
        assertFalse(tree.delete(1));
        assertFalse(tree.contains(1));

        assertTrue(tree.delete(2));
        assertFalse(tree.delete(2));
        assertFalse(tree.contains(2));
    }

    @Test
    @DisplayName("Exceptional tests")
    public void exceptionTests() {
        assertThrows(UnsupportedOperationException.class, () -> {
            Parent rp = tree.getRootParent();
            rp.updateLeft(null);
        });
    }

}
