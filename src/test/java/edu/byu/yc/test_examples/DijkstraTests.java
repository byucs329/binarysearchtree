package edu.byu.yc.test_examples;

import static edu.byu.yc.test_examples.Dijkstra.M;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.stream.Stream;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.DynamicNode;
import org.junit.jupiter.api.DynamicTest;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestFactory;

@DisplayName("Tests for the implementation of Dijkstra's algorithm")
public class DijkstraTests {
    private final int[][] graph = { { 0, 2, 4, M, M, M }, { 2, 0, 1, 4, M, M },
            { 4, 1, 0, 2, 6, M }, { M, 4, 2, 0, 1, 3 }, { M, M, 6, 1, 0, 5 },
            { M, M, M, 3, 5, 0 } };
    private final int[][] reference = { { 0, 2, 3, 5, 6, 8 },
            { 2, 0, 1, 3, 4, 6 }, { 3, 1, 0, 2, 3, 5 }, { 5, 3, 2, 0, 1, 3 },
            { 6, 4, 3, 1, 0, 4 }, { 8, 6, 5, 3, 4, 0 } };
    private final String graphString = String.format(
            "0 2 4 - - -%n2 0 1 4 - -%n4 1 0 2 6 -%n- 4 2 0 1 3%n- - 6 1 0 5%n- - - 3 5 0");
    private final String referenceString = String.format(
            "0 2 3 5 6 8%n2 0 1 3 4 6%n3 1 0 2 3 5%n5 3 2 0 1 3%n6 4 3 1 0 4%n8 6 5 3 4 0");

    @TestFactory
    @DisplayName("End to end")
    public Stream<DynamicNode> endToEnd() {

        final int cells = graph.length * graph.length;

        Dijkstra d = new Dijkstra(graph);
        ArrayList<DynamicNode> tests = new ArrayList<>(cells + 1);

        int count = 0;

        for (int i = 0; i < graph.length; ++i) {
            for (int j = 0; j < graph.length; ++j) {
                ++count;
                final int test_i = i;
                final int test_j = j;
                tests.add(DynamicTest.dynamicTest(
                        "Checking shortest path from " + i + " to " + j, () -> {
                            assertEquals(reference[test_i][test_j],
                                    d.shortestPath(test_i, test_j));
                        }));
            }
        }

        final int finalCount = count;

        tests.add(DynamicTest
                .dynamicTest("Should have created " + cells + " tests", () -> {
                    assertEquals(cells, finalCount);
                }));

        return tests.stream();
    }

    @Test
    @DisplayName("String output - original graph")
    public void originalGraphStringOutput() {
        assertEquals(graphString, Dijkstra.tableToString(graph));
    }

    @Test
    @DisplayName("String output - reference graph")
    public void referenceGraphStringOutput() {
        assertEquals(referenceString, Dijkstra.tableToString(reference));
    }
}
