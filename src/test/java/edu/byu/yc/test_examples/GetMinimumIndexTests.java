package edu.byu.yc.test_examples;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

@DisplayName("getMinimumIndex tests")
class GetMinimumIndexTests {

    @Test
    @DisplayName("empty array; empty set")
    public void getMinimumIndexEmptyEmpty() {
        int[] a = {};
        assertEquals(Dijkstra.M, Dijkstra.getMinimumIndex(a, new BSTSet()));
    }

    @Test
    @DisplayName("non-empty array; empty set")
    public void getMinimumIndexNonemptyEmpty() {
        int[] a = { 1, 2 };
        assertEquals(0, Dijkstra.getMinimumIndex(a, new BSTSet()));
    }

    @Test
    @DisplayName("non-empty array and set; all elements in set")
    public void getMinimumIndexNonemptyNonemptyNothingUnique() {
        int[] a = { 1 };
        BSTSet s = new BSTSet();
        s.add(0);
        assertEquals(Dijkstra.M, Dijkstra.getMinimumIndex(a, s));
    }

    @Test
    @DisplayName("non-empty array and set; some elements in set")
    public void getMinimumIndexNonemptyNonemptyPartiallyUnique() {
        int[] a = { 1, 2 };
        BSTSet s = new BSTSet();
        s.add(1);
        assertEquals(0, Dijkstra.getMinimumIndex(a, s));
        s.delete(1);
        s.add(0);
        assertEquals(1, Dijkstra.getMinimumIndex(a, s));
    }
}