package edu.byu.yc.test_examples;

public class BSTSet {
    interface Parent {
        public Node getChild();

        public void updateLeft(Node n);

        public boolean delete();

        public boolean newChild(int v);
    }

    class RootParent implements Parent {
        @Override
        public Node getChild() {
            return root;
        }

        @Override
        public boolean delete() {
            if (root == null) {
                return false;
            } else {
                Node r = root.delete();
                root = r;
                return true;
            }
        }

        @Override
        public void updateLeft(Node n) {
            throw new UnsupportedOperationException("Root can't appear here");
        }

        @Override
        public boolean newChild(int v) {
            if (root == null) {
                root = new Node(v);
                return true;
            } else {
                return false;
            }
        }
    }

    class ChildParent implements Parent {
        public final Node p;
        public final boolean left;

        public ChildParent(Node parent, boolean l) {
            p = parent;
            left = l;
        }

        @Override
        public Node getChild() {
            if (left) {
                return p.l;
            } else {
                return p.r;
            }
        }

        @Override
        public boolean delete() {
            Node child = getChild();
            if (child == null) {
                return false;
            } else {
                Node result = child.delete();
                if (left) {
                    p.l = result;
                } else {
                    p.r = result;
                }
                return true;
            }
        }

        @Override
        public void updateLeft(Node n) {
            p.l = n;
        }

        @Override
        public boolean newChild(int v) {
            if (getChild() == null) {
                Node child = new Node(v);
                if (left) {
                    p.l = child;
                } else {
                    p.r = child;
                }
                return true;
            } else {
                return false;
            }
        }
    }

    class Node {
        public Node(int value) {
            v = value;
            l = null;
            r = null;
        }

        public Node(int value, Node left, Node right) {
            v = value;
            l = left;
            r = right;
        }

        public final int v;
        private Node l;
        private Node r;

        private Parent find(int value) {
            Node child;
            boolean lt = value < v;
            if (lt) {
                child = l;
            } else {
                child = r;
            }
            if (child == null || child.v == value) {
                return new ChildParent(this, lt);
            } else {
                return child.find(value);
            }
        }

        public Node delete() {
            if (l == null) {
                return r;
            } else {
                if (r == null) {
                    return l;
                } else {
                    // arbitrarily delete the right child
                    Parent nextParent = r.findMin();
                    if (nextParent == null) {
                        return new Node(r.v, l, r.r);
                    } else {
                        Node next = nextParent.getChild();
                        Node newMin;
                        if (next.r == null) {
                            newMin = null;
                        } else {
                            newMin = new Node(next.r.v, null, next.r.r);
                        }
                        nextParent.updateLeft(newMin);
                        return new Node(next.v, l, r);
                    }
                }
            }
        }

        private Parent findMin() {
            if (l == null) {
                return null;
            } else {
                if (l.l == null) {
                    return new ChildParent(this, true);
                } else {
                    return l.findMin();
                }
            }
        }
        
        public int size() {
            int lSize = 0;
            if (l != null) {
                lSize = l.size();
            }
            int rSize = 0;
            if (r != null) {
                rSize = r.size();
            }
            return 1 + lSize + rSize;
        }
    }

    public BSTSet() {
        root = null;
    }

    private Node root;

    public boolean contains(int v) {
        return find(v).getChild() != null;
    }

    public boolean add(int v) {
        return find(v).newChild(v);
    }

    public boolean delete(int v) {
        Parent p = find(v);
        return p.delete();
    }

    private final RootParent rootParent = new RootParent();

    private Parent find(int value) {
        if (root == null || root.v == value) {
            return rootParent;
        } else {
            return root.find(value);
        }
    }

    public Parent getRootParent() {
        return rootParent;
    }

    public int size() {
        if (root == null) {
            return 0;
        } else {
            return root.size();
        }
    }
}
